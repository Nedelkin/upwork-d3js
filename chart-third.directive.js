angular.module('upwork-d3js').directive('chartThird', function() {
  return {
    scope: {
      data: "="
    },
    template: `<svg class="chart-third"></svg>`,
    link: (scope) => {
      let data = scope.data || [
          {
            label:  "paid",
            value: 9450,
          },
          {
            label: "overdue",
            value: 2169,
          }, {
            label: "open invoices",
            value: 5317
          }];

      let color = d3.scale.category20()
      let chartWidth     = 200,
        barHeight        = 30;


      let chart = d3.select(".chart-third")
        .attr("width", 200)
        .attr("height", chartWidth);

      let bar = chart.selectAll("g")
        .data(data)
        .enter().append("g")
        .attr("transform", (d, i) => `translate(0, ${getY(i)})`);

      bar.append("rect")
        .attr("fill", (d,i) => color(i))
        .attr("class", "bar")
        .attr("width", barHeight - 1)
        .attr("height", (d, i) => {
          let sum = data.map((obj) => obj.value)
            .reduce((sum, current) => sum + current, 0);
          let part = data[i].value/sum;
          return chartWidth * part;

        })
        .attr("my-data", (d, i) => d.label);

      let legendRectSize = 25,
        legendSpacing  = 4;

      let legend = chart.selectAll('.legend')
        .data(data)
        .enter()
        .append('g')
        .attr('transform', (d, i) => `translate(${30},${25 + 50*i})`);

      legend.append("text")
        .attr("class", "label")
        .attr("x", legendRectSize + legendSpacing + 15)
        .attr("y", legendRectSize - legendSpacing - 7)
        .text((d, i) => `$${data[i].value}`);

      legend.append("text")
        .attr("class", "label1")
        .attr("x", legendRectSize + legendSpacing + 15)
        .attr("y", legendRectSize - legendSpacing + 15)
        .text((d, i) => `${data[i].label}`);

      function getY(i) {
        let sum = data.map((obj) => obj.value)
          .reduce((sum, current) => sum + current, 0);

        let parts = data.map((obj => chartWidth * (obj.value/sum)));
        let y = 0;
        for (let k = i + 1; k < data.length; k++) {
          y += parts[k];
        }
        return y;
      }


    }}
});