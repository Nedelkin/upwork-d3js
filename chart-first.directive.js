angular.module('upwork-d3js').directive('chartFirst', function() {
  return {
    scope: {
      data: "=",
    },
    template: `
      <svg class="chart"></svg>
      `,
    link:(scope) => {
      let data = scope.data || [
          {
            label:  [23232, 'income'],
            value: [23232],
            color: "#00b26e"
          },
          {
            label: [2323, 'expenses'],
            value: [2323],
            color: "#00c0cc"
          }];


      let chartWidth       = 200,
          barHeight        = 30,
          gapBetweenGroups = 30,
          spaceForLabels   = 150;

      let zippedData = data.map((obj) => obj.value);

      let chartHeight = barHeight * zippedData.length + gapBetweenGroups * data.length;

      let x = d3.scale.linear()
                      .domain([0, d3.max(zippedData)])
                      .range([0, chartWidth]);

      let y = d3.scale.linear()
                      .range([chartHeight + gapBetweenGroups + 100, 100]);


      let chart = d3.select(".chart")
                    .attr("width", spaceForLabels + chartWidth)
                    .attr("height", chartHeight);

      let bar = chart.selectAll("g")
                    .data(zippedData)
                    .enter().append("g")
                    .attr("transform", (d, i) => "translate(0," + (i * 2 * barHeight) + ")");

      bar.append("rect")
         .attr("fill", (d,i) => data[i].color)
         .attr("class", "bar")
         .attr("width", x)
         .attr("height", barHeight - 1);

      bar.append("text")
        .attr("class", "label")
        .attr("x", (d) => x(d) + 30)
        .attr("y", barHeight / 2)
        .attr("dy", "-.20em")
        .text((d, i) => `$${data[i].label[0]}`);

      bar.append("text")
        .attr("class", "label1")
        .attr("x", (d) => x(d) + 30)
        .attr("y", barHeight)
        .attr("dy", ".20em")
        .text((d, i) => `${data[i].label[1]}`);
    }
  }
});
