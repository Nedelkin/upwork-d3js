angular.module('upwork-d3js').directive('chartSecond', function() {
  return {
    scope: {
      data: "="
    },
    template: `
      <svg class="chart-second"></svg>
    `,
    link: (scope) => {
      //data
      let data = scope.data || [
          {
            value: 42,
            label: "all"
          }, {
            value: 6,
            label: "completed"
          }, {
            value: 2,
            label: "in progress"
          }, {
            value: 1,
            label: "open"
          }
        ];

      let color = d3.scale.category20(),
          width = 350,
          height = 200,
          radius = Math.min(width, height) / 2;

      let pie = d3.layout.pie()
                  .value((d) => d.value)
                  .sort(null);

      let arc = d3.svg.arc()
                  .innerRadius(radius)
                  .outerRadius(radius - 30);

      let chart = d3.select(".chart-second")
                    .attr("width", width + 200)
                    .attr("height", height)
                    .append("g")
                    .attr("transform", "translate(" + width / 3 + "," + height / 2 + ")");

      let path = chart.datum(data).selectAll("path")
                      .data(pie)
                      .enter().append("path")
                      .attr("class", "arc")
                      .attr("fill", (d, i) => color(i))
                      .attr("d", arc);

      let legendRectSize = 25,
          legendSpacing  = 4;

      let legend = chart.selectAll('.legend')
                        .data(data)
                        .enter()
                        .append('g')
                        .attr('transform', (d, i) => `translate(${130},${- 120 + 50*i})`);

      legend.append('rect')
            .attr('width', legendRectSize)
            .attr('height', legendRectSize)
            .style('fill', (d, i) => color(i))
            .style('stroke', (d, i) => color(i));


      legend.append("text")
            .attr("class", "label")
            .attr("x", legendRectSize + legendSpacing + 15)
            .attr("y", legendRectSize - legendSpacing - 7)
            .attr("dy", "-.20em")
            .text((d, i) => `${data[i].value}`);  

      legend.append("text")
        .attr("class", "label1")
        .attr("x", legendRectSize + legendSpacing + 15)
        .attr("y", legendRectSize - legendSpacing + 7)
        .attr("dy", ".20em")
        .text((d, i) => `${data[i].label}`);
    }
  }
});